<?php
namespace App\Http\Controllers;
use App\Models\Pengguna;
use Illuminate\Http\Request;

class UserController extends Controller {
    function show($id) {
        $msg =  "I got this $id <br>";
        $users = Pengguna::all(); // return array of Pengguna
        // foreach($users as $u) {
        //     echo "Nama: $u->name Email: $u->email <br>";
        // }
        return view('pengguna', compact('users', 'msg'));
    }

    function create() {
        $user = new Pengguna();
        return view('user_form', compact('user'));
    }

    function save(Request $req) {
        // 1. read the data from form
        $name  = $req->name;
        $email = $req->email;
        $id    = $req->id;
        echo "$name $email";

        // 2. validate the data
        $req->validate([
            'name'  => 'required',
            'email' => 'required'
        ]);

        // 3. save data to DB
        if(empty($id)) {
            // insert
            $user = new Pengguna();
        } else {
            // update
            $user = Pengguna::find($id);
        }
        $user->name = $name;
        $user->email = $email;
        $user->save(); // insert OR update into table users
        return redirect('/user-details/xxxx');
    }

    function edit($id) {
        $user = Pengguna::find($id); // find() cari by pk and return an object Pengguna
        return view('user_form', compact('user'));
    }

    function delete($id) {
        //Pengguna::find($id)->delete();
        $user = Pengguna::find($id);
        $user->delete();
        return redirect('/user-details/xxx');
    }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller {
    // show login screen
    function login() {
        return view('login.form'); // views/login/form.blade.php
    }

    function auth(Request $req) {
        $user_id  = $req->user_id;
        $password = $req->password;
        //echo "$user_id $password";
        $credentials = ['user_id' => $user_id, 'password' => $password];

        if(Auth::attempt($credentials)) {
            // berjaya
            //echo "Berjaya";
            $req->session()->flash('msg', 'Selamat datang ' . \Auth::user()->name); // flash session
            session(['user_id' => $user_id]); // set data ke dlm session

            return redirect('/user-details/xxx');
        } else {
            // gagal
            //echo "Gagal";
            $msg = "Login Gagal...";
            return view('login.form', compact('msg'));
        }
    }

    function logout() {
        \Auth::logout(); // kill all session
        session()->flush(); // destroy all sessions
        return redirect('/login');
    }
}

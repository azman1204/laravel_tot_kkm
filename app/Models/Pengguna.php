<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

// satu model mewakili satu table
class Pengguna extends Model {
    protected $table = 'users';
}

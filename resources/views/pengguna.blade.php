@extends('layout.app')
@section('title', 'User List')
@section('body')
    <h4>User List</h4>
    {!! $msg !!}
    <a href="/user-create" class="btn btn-primary btn-sm mb-1">New User</a>
    <table class="table table-striped table-hover">
        <thead>
            <tr class="table-info">
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <?php $no = 1; ?>
        @foreach ($users as $u)
            <tr>
                <td>{{ $loop->index + 1 }} {{ $no++ }}</td>
                <td>{{ $u->name }}</td>
                <td>{{ $u->email }}</td>
                <td>
                    <a href="/user-edit/{{ $u->id }}" class="btn btn-success btn-sm">Edit</a>
                    <a href="/user-delete/{{ $u->id }}"
                        class="btn btn-danger btn-sm"
                        onclick="return confirm('Are you sure ?')">Delete</a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection

@section('js')
    <script>
        //alert('Hello');
    </script>
@endsection

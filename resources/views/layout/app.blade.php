<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>@yield('title')</title>
</head>
<body>
    <div id="my-wrapper">
        <div id="my-header">
            <a href="/">Home</a> |
            <a href="/user-details/xxx">User List</a> |
            <a href="/logout">Log Out ({{ \Auth::user()->name }} - {{ session('user_id') }})</a>
        </div>
        <div id="my-content">

            @if(session()->has('msg'))
                <div class="alert alert-success">{{ session('msg') }}</div>
            @endif

            @yield('body', 'Laravel Tutorial')
        </div>
        <div id="my-footer"></div>
    </div>

    <style>
        #my-wrapper {
            border: 4px solid #999;
        }

        #my-header {
            border: 2px solid red;
        }

        #my-content {
            border: 1px solid green;
        }

        #my-footer {
            border: 4px solid yellow;
        }
    </style>

    @yield('js')
</body>
</html>

@extends('layout.app')
@section('body')
<h4>New User</h4>

@if($errors->any())
    @foreach($errors->all() as $err)
        {{ $err }} <br>
    @endforeach
@endif

<form action="/user-save" method="post">
    <input type="hidden" name="id" value="{{ $user->id }}">
    @csrf
    <label>Name</label>
    <input type="text" value="{{ $user->name }}" name="name" class="form-control">
    <label>Email</label>
    <input type="text" value="{{ $user->email }}" name="email" class="form-control">
    <input type="submit" class="btn btn-primary" value="Save">
</form>
@endsection

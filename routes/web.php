<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;

// upload file
Route::view('/upload', 'upload'); // views/upload.blade.php

Route::post('/upload', function(Request $req) {
    $name = $req->file('avatar')->getClientOriginalName(); // return nama asal file yg di upload. cth: cat.png
    echo $name;
    $req->file('avatar')->storeAs('upload/', $name); // save ke folder storage/app/upload
    // param1 = nama folder, param2 = nama file
});

Route::get("/download", function() {
    $path = storage_path() . "/app/upload/Server Installation.docx"; // C:\....\storage
    echo $path;
    return response()->download($path);
});


Route::middleware(['first'])->group(function() {
    // route trigger controller
    // http://laravel_tot.test/user-details/100
    Route::get('/user-details/{id}', [UserController::class, 'show']);
    // on click btn 'new user'
    Route::get('/user-create', [UserController::class, 'create']);
    // on click btn 'save'
    Route::post('/user-save', [UserController::class, 'save']);
    // on click btn 'edit
    Route::get('/user-edit/{id}', [UserController::class, 'edit']);
    // on click btn 'delete'
    Route::get('/user-delete/{id}', [UserController::class, 'delete']);
});

// send email
Route::get('/send-email', function() {
    //\Mail::to('azman1204@yahoo.com')->subject('Hello World')->send();
    // sama mcm .. return view('nama view', compact())
    $nama = "Azman";
    // views/my_email.blade.php
    \Mail::send('my_email', compact('nama'), function($message) {
        $message->to('azman1204@yahoo.com')->subject('Hello World');
    });
});





// login
Route::get('/login', [LoginController::class, 'login']);
Route::post('/login', [LoginController::class, 'auth']);
Route::get('/logout', [LoginController::class, 'logout']);
// hash kan password
Route::get('/password', function() {
    return \Hash::make('1234');
});


Route::get('/', function () {
    return view('welcome');
});

// hello.html - URL pattern
// http://laravel_tot.test/hi
Route::get('/hi', function() {
    return view('hello'); // resources/views/hello.blade.php
});

// route yg ada data
// http://laravel_tot.test/user/azman
Route::get('/user/{nama}', function($nama) {
    echo "Nama anda $nama";
});
